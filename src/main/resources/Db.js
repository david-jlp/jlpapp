##### COMMANDS TO CREATE A SAMPLE DATABASE & COLLECTION #####
> use mydb

> db.mycollection.insertMany( [
	{ "id" : "101", "name" : "Daniel Atlas" }, 
	{ "id" : "102", "name" : "Charlotte Neil" },
	{ "id" : "97", "name" : "tom jackmen" }
] )

##### COMMAND TO DISPLAY THE DOCUMENTS OF A COLLECTION #####
> db.mycollection.find()


> db.partner.insertMany( [
	{ "id" : "1", "username" : "dahveed", "firstname" : "David", "lastname" : "John", "password" : "123456", "email" : "a@b.com", "address" : "Kharadi", "phone" : "12345678",  }, 
	{ "id" : "2", "username" : "suhas", "firstname" : "Suhas", "lastname" : "Pindiproli", "password" : "123456", "email" : "s@p.com", "address" : "Kharadi", "phone" : "12345678",  }
] )

> db.partner.find()