package com.jlp.app;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jlp.app.mongo.factory.MongoFactory;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

@Service("partnerService")
@Transactional
public class PartnerService {

	static String db_name = "mydb", db_collection = "partner";
	private static Logger log = Logger.getLogger(PartnerService.class);

	// Fetch all partners from the mongo database.
	public List<Partner> getAll() {
		List<Partner> partner_list = new ArrayList<Partner>();
		DBCollection coll = MongoFactory.getCollection(db_name, db_collection);

		// Fetching cursor object for iterating on the database records.
		DBCursor cursor = coll.find();
		while (cursor.hasNext()) {
			DBObject dbObject = cursor.next();

			Partner partner = new Partner();
			partner.setId(dbObject.get("id").toString());
			partner.setAddress(dbObject.get("address").toString());
			partner.setEmail(dbObject.get("email").toString());
			partner.setFirstname(dbObject.get("firstname").toString());
			partner.setLastname(dbObject.get("lastname").toString());
			partner.setPassword(dbObject.get("password").toString());
			partner.setPhone(dbObject.get("phone").toString());
			partner.setUsername(dbObject.get("username").toString());

			// Adding the user details to the list.
			partner_list.add(partner);
		}
		log.debug("Total records fetched from the mongo database are= " + partner_list.size());
		return partner_list;
	}

	// Add a new user to the mongo database.
	public Boolean add(Partner partner) {
		boolean output = false;
		Random ran = new Random();
		log.debug("Adding a new partner to the mongo database; Entered user_name is= " + partner.getUsername());
		try {
			DBCollection coll = MongoFactory.getCollection(db_name, db_collection);

			// Create a new object and add the new user details to this object.
			BasicDBObject doc = new BasicDBObject();
			doc.put("id", String.valueOf(ran.nextInt(100)));
			doc.put("address", partner.getAddress());
			doc.put("email", partner.getEmail());
			doc.put("firstname", partner.getFirstname());
			doc.put("lastname", partner.getLastname());
			doc.put("password", partner.getPassword());
			doc.put("phone", partner.getPhone());
			doc.put("username", partner.getUsername());

			// Save a new user to the mongo collection.
			coll.insert(doc);
			output = true;
		} catch (Exception e) {
			output = false;
			log.error("An error occurred while saving a new user to the mongo database", e);
		}
		return output;
	}

	public Partner findPartnerId(String username) {
		Partner p = new Partner();
		DBCollection coll = MongoFactory.getCollection(db_name, db_collection);

		// Fetching the record object from the mongo database.
		DBObject where_query = new BasicDBObject();
		where_query.put("username", username);

		DBObject dbo = coll.findOne(where_query);
		if (dbo != null) {
			p.setId(dbo.get("id").toString());
			p.setAddress(dbo.get("address").toString());
			p.setEmail(dbo.get("email").toString());
			p.setFirstname(dbo.get("firstname").toString());
			p.setLastname(dbo.get("lastname").toString());
			p.setPassword(dbo.get("password").toString());
			p.setPhone(dbo.get("phone").toString());
			p.setUsername(dbo.get("username").toString());
		}
		// Return user object.
		return p;
	}

	public void register(Partner partner) {
		// TODO Auto-generated method stub
		add(partner);

	}

	public Partner validateUser(Login login) {

		Partner partner = findPartnerId(login.getUsername());
		if (login.getPassword().equals(partner.getPassword())) {
			return partner;
		}

		return null;

	}

}
